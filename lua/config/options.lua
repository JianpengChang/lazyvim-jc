-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

vim.g.autoformat = false
vim.opt.mouse = "" -- disable mouse

local opt = vim.opt

opt.wrap = true

-- set spcace to -
opt.list = false
opt.listchars = "tab:>-,trail:-"

-- Tab
opt.tabstop = 4 -- number of visual spaces per TAB
opt.softtabstop = 4 -- number of spacesin tab when editing
opt.shiftwidth = 4 -- insert 4 spaces on a tab
opt.expandtab = true -- tabs are spaces, mainly because of python

-- UI config
opt.number = true -- show absolute number
opt.relativenumber = false -- add numbers to each line on the left side
opt.cursorline = true -- highlight cursor line underneath the cursor horizontally
-- opt.splitbelow = true -- open new vertical split bottom
-- opt.splitright = true -- open new horizontal splits right
